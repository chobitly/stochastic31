package com.lu.stochastic31.content;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import android.os.Environment;

class FileHelper {
	static final String DIR = Environment.getExternalStorageDirectory()
			.getPath() + "/stochastic31/";
	static final String END_SUFFIX = ".random";

	/** SD卡是否存在 **/
	public static boolean hasSD = Environment.getExternalStorageState().equals(
			android.os.Environment.MEDIA_MOUNTED);

	public static String[] listTitles() {
		String[] filenames = listFiles();
		String[] titles = new String[filenames.length];
		for (int i = 0; i < filenames.length; ++i)
			titles[i] = filenames[i].replace(END_SUFFIX, "");
		return titles;
	}

	public static String[] listFiles() {
		if (hasSD)
			return listFiles(DIR, END_SUFFIX);
		return new String[] {};
	}

	public static String[] listFiles(String dir, final String end_suffix) {
		File f = new File(dir);
		if (!f.exists())
			f.mkdirs();
		String[] filenames = f.list(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String filename) {
				return filename.indexOf(end_suffix) > 0
						&& filename.endsWith(end_suffix.substring(1));
			}
		});
		return filenames;
	}

	/**
	 * 创建文件对象
	 */
	public static File getFileInstance(String filename, boolean onSD) {
		if (onSD && hasSD)
			return new File(DIR + filename);
		return new File(filename);
	}

	/**
	 * 创建文件
	 * 
	 * @throws IOException
	 *             因为某些原因无法成功创建文件
	 */
	public static File createFile(File file) throws IOException {
		if (!file.exists())
			file.createNewFile();
		return file;
	}

	/**
	 * 删除文件
	 * 
	 * @param item
	 * @return 是否成功删除文件
	 */
	public static boolean deleteFile(File file) {
		if (file == null || !file.exists() || file.isDirectory())
			return false;
		return file.delete();
	}

	/**
	 * 读取文件
	 * 
	 * @param file
	 * @param canRepeat
	 *            是否允许重复
	 * @return 是否成功读取文件
	 */
	public static ArrayList<String> readFile(File file, boolean canRepeat) {
		ArrayList<String> arrayContent = new ArrayList<String>();
		try {
			String temp;
			BufferedReader br = new BufferedReader(new InputStreamReader(
					new FileInputStream(file), "utf-8"));
			arrayContent.clear();
			while ((temp = br.readLine()) != null) {
				temp = temp.trim();// 去掉前后空格
				if (temp.length() > 0)// 滤掉空行
					if (canRepeat || !arrayContent.contains(temp))// 滤掉重复
						arrayContent.add(temp);
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return arrayContent;
	}

	/**
	 * 重写文件
	 * 
	 * @param file
	 * @param content
	 * @return 是否成功重写文件
	 */
	public static boolean writeFile(File file, ArrayList<String> content) {
		if (file == null || content == null)
			return false;
		try {
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(file), "utf-8"));
			for (String str : content)
				out.write(str);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * 续写文件
	 * 
	 * @param file
	 * @param content
	 * @return 是否成功续写文件
	 */
	public static boolean appendFile(File file, String appendStr) {
		if (file == null || appendStr == null)
			return false;
		try {
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(file, true), "utf-8"));
			out.append(appendStr);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
