package com.lu.stochastic31;

import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;

import android.app.Application;

@ReportsCrashes(formUri = "http://collector.tracepot.com/72403375")
// acra采用默认模式(SILENT)，只增加了后台的bug tracing
public class MainApplication extends Application {

	public void onCreate() {
		super.onCreate();
		// The following line triggers the initialization of ACRA
		ACRA.init(this);
	}

}